var load_screen = document.getElementById("load_screen");

window.addEventListener('load', function() {
    
    // document.body.removeChild(load_screen);
    document.getElementById('load_screen').style.display = 'none';
});

var camera;
var scene;
var renderer;

function init() {

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(45, window.innerWidth/window.innerHeight, 0.1, 1000);
    
    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true, clearAlpha: 1 });

    //renderer.setClearColor(0x9575cd);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMapEnabled = true;
    renderer.autoClear = false;
    renderer.setClearColor(0x000116, 1.0);

    document.getElementById('WebGL-output').appendChild(renderer.domElement);

    var stats = initStats();

    loader = new THREE.JSONLoader();
    loader.load( '../assets/komeet.json', addModel );

    function addModel( geometry,  materials ){
        var material = new THREE.MeshFaceMaterial( materials );
        model = new THREE.Mesh( geometry, material );
        model.scale.set (7,7,7);
        model.position.set (0,0,0);

        scene.add( model );            
    }

    var clock = new THREE.Clock();

    var renderPass = new THREE.RenderPass(scene, camera);
    var effectFilm = new THREE.FilmPass(12.6, 0.005, 256, false);
    effectFilm.renderToScreen = true;

    var composer = new THREE.EffectComposer(renderer);
    composer.addPass(renderPass);
    composer.addPass(effectFilm)


    // var listener1 = new THREE.AudioListener();
    // camera.add(listener1);
    // var sound1 = new THREE.Audio(listener1);
    // sound1.load('../assets/audio/audio.mp3');
    // sound1.setRefDistance(20);
    // sound1.setLoop(true);
    // sound1.setRolloffFactor(1);

    // cube.add(sound1);

    var spotLight = new THREE.SpotLight( 0xfff );
    spotLight.position.set( -40, 60, -10 );
    spotLight.castShadow = true;

    scene.add(spotLight);

    //scene.overrideMaterial = new THREE.MeshLambertMaterial({color: 0xffffff});

    // add a small sphere simulating the pointlight
    var sphereLight = new THREE.SphereGeometry(0.2);
    var sphereLightMaterial = new THREE.MeshBasicMaterial({color: 0x6200ea});
    var sphereLightMesh = new THREE.Mesh(sphereLight, sphereLightMaterial);
    sphereLightMesh.castShadow = true;

    sphereLightMesh.position = new THREE.Vector3(3, 0, 3);
    scene.add(sphereLightMesh);

    var pointColor = "#aa00ff";
    var pointLight = new THREE.PointLight(pointColor);
    pointLight.distance = 25;
    pointLight.intensity = 10;
    scene.add(pointLight);

    // camera.position.x = -30;
    // camera.position.y = 40;
    // camera.position.z = 25;
    
    document.getElementById("WebGL-output").appendChild(renderer.domElement);

    var controls = new function() {
        this.rotationSpeed = 0.015;
        this.pointColor = pointColor;
        this.intensity = 10;
        this.distance = 50;
        this.bouncingSpeed = 0.03;
        this.positionX = 0;
        this.positionY = 0;
        this.positionZ = 0;
        this.cameraX = -8;
        this.cameraY = -1;
        this.cameraZ = 20;
    }



    var gui = new dat.GUI();
    gui.add(controls, 'rotationSpeed', 0, 0.5);

    gui.addColor(controls, 'pointColor').onChange(function (e) {
        pointLight.color = new THREE.Color(e);
    });

    gui.add(controls, 'intensity', 0, 15).onChange(function (e) {
        pointLight.intensity = e;
    });

    gui.add(controls, 'distance', 0, 100).onChange(function (e) {
        pointLight.distance = e;
    });

    gui.add(controls, 'positionX', -10, 10).onChange(function (e) {
        cube.position.x = e;
    });
    gui.add(controls, 'positionY', -10, 10).onChange(function (e) {
        cube.position.y = e;
    });
    gui.add(controls, 'positionZ', -10, 10).onChange(function (e) {
        cube.position.z = e;
    });

    gui.add(controls, 'cameraX', -50, 50).onChange(function (e) {
        camera.position.x = e;
    });
    gui.add(controls, 'cameraY', -50, 50).onChange(function (e) {
        camera.position.y = e;
    });
    gui.add(controls, 'cameraZ', -50, 50).onChange(function (e) {
        camera.position.z = e;
    });

    camera.position.x += controls.cameraX;
    camera.position.y += controls.cameraY;
    camera.position.z += controls.cameraZ;

    renderScene();

    var step = 0;
    var invert = 1;
    var phase = 0;
    var model;

    var ratKitchen = document.querySelector('.comingsoon'),
    aliG = document.querySelector('.h1__wrap');

    var wrappie = document.querySelector('.h1__wrap'),
        play = document.querySelector('.p__wrap');

    var fademuchTl = new TimelineMax({paused:true}); 

        fademuchTl.from('.h1__wrap', 1.4, { delay: 0.1, autoAlpha:1, ease: Power4.easeInOut }, 0)
                .to(camera.position, 2, {y: 50, x: 50, z: 100, ease: Power4.easeInOut }, 0)
        ;

    var fademuchTl2 = new TimelineMax({paused:true}); 

        fademuchTl2.to(camera.position, 2, {y: 1, x: 11, z: 30, ease: Power4.easeInOut }, 0)
        ;

    play.addEventListener('click', function(event) {
        event.preventDefault(); 
        fademuchTl.reversed() ? fademuchTl.play() : fademuchTl.reverse();
    }); 

    wrappie.addEventListener('click', function(event) {
        event.preventDefault();  
        fademuchTl2.reversed() ? fademuchTl2.play() : fademuchTl2.reverse();
    });  

    function renderScene() {
        stats.update(); 

        if (model) model.rotation.y += 0.0005;
        //camera.lookAt(scene.position);

        //move the light simulation
        if (phase > 2 * Math.PI) {
            invert = invert * -1;
            phase -= 2 * Math.PI;
        } else {
            phase += controls.rotationSpeed;
        }
        // sphereLightMesh.position.x = 20 + ( 10 * (Math.cos(step)));
        // sphereLightMesh.position.y = 2 + ( 10 * Math.abs(Math.sin(step)));

        step += controls.bouncingSpeed;
        sphereLightMesh.position.y = (27 * (Math.cos(phase)));
        sphereLightMesh.position.x = (27 * (Math.sin(phase)));
        //sphereLightMesh.position.y = (4 * Math.tan(step));

        var delta = clock.getDelta();

        pointLight.position.copy(sphereLightMesh.position);

        requestAnimationFrame(renderScene);
        renderer.render(scene, camera);
        composer.render(delta);
    }

    function initStats() {
        var stats = new Stats();
        stats.setMode(0);
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.left = '0px';
        stats.domElement.style.top = '0px';
        document.getElementById("Stats-output").appendChild( stats.domElement );
        return stats;
    }

}
function onResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
}

window.onload = init

window.addEventListener('resize', onResize, false);





$(document).ready(function() {
    $('#fullpage').fullpage();
});

